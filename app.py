import json
from flask import Flask, request

app = Flask(__name__)


@app.route('/api/eligible', methods=["POST"])
def tasks():
    eligible = False
    request_data = request.get_json()
    if request_data["age"] > 18:
        eligible = True
    return {
        "eligible": eligible
    }


@app.route('/api/age', methods=['POST'])
def create_task():
    age = 10
    return {
        "age": age
    }
